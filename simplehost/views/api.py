#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
api.py
~~~~~~

:copyright: (c) 2018 by sabmek
:license: see LICENSE for more details.
"""


import requests
import flask
from flask import session, jsonify
from flask.views import MethodView
from api import models
from views import search

PRIVATE_ENDPOINTS = []

class Router(MethodView):

    def get(self, cls, _id=None):
        if not models.core.models.get(cls):
            return {"error": "Invalid endpoint"}
        if request.args.get('action') == 'search':
            return {cls: [r.dict() for r in search(models.core.models[cls])]}
        if _id:
            return models.core.models[cls].get(_id).dict(minimal=False)
        return {cls: [v.dict(minimal=True) for v in models.core.models[cls].all()]}

class Index(MethodView):

    def get(self):
        return jsonify({"endpoints": list(set(models.core.models.keys() - set(PRIVATE_ENDPOINTS)))})
