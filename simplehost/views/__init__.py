#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
__init__.py
~~~~~~~~~~~

:copyright: (c) 2018 by sabmek
:license: see LICENSE for more details.
"""

from flask import render_template, request
from flask.views import MethodView


class Base(MethodView):
    def get(self, uri=None):
        return render_template('index.html')


class Partial(MethodView):
    def get(self, partial):
        return render_template('partials/%s.html' % partial)

    
def search(model, limit=50, lazy=True):
    query = request.args.get('query')
    field = request.args.get('field')
    limit = min(int(request.args.get('limit', limit)), limit)
    if all([query, field, limit]):
        return model.search(query, field=field, limit=limit, lazy=lazy)
    raise ValueError('Query and field must be provided. Valid fields are: %s' \
                    %  model.__table__.columns.keys())
