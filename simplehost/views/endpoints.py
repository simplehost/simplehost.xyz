#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
endpoints.py
~~~~~~~~~~~~

:copyright: (c) 2018 by sabmek
:license: see LICENSE for more details.
"""

import time
import requests
import flask
from flask.views import MethodView
from flask import render_template
import api.uploader
import api.models

SLEEP_FOR = 15
SIMPLEHOST = 'http://simplehost.xyz/p'
HOST = 'https://archive.org/download/simplehost.xyz'


def our_render(template, **kwargs):
    default_kwargs = {
            'account': None
    }
    full_kwargs = default_kwargs.copy()
    full_kwargs.update(kwargs)
    return render_template(template, **full_kwargs)


class Host(MethodView):

    def post(self, sleep=SLEEP_FOR):
        fullpath = flask.request.form.get('domain')
        html = flask.request.form.get('html')
        passcode = flask.request.form.get('passcode')

        if '/' in fullpath:
            domain, path = fullpath.split('/', 1)
        else:
            domain, path = fullpath, 'index.html'

        api.uploader.upload(domain, html, filepath=path, passcode=passcode)

        time.sleep(sleep)  # terrible tmp hack

        url = '{}/{}/{}'.format(SIMPLEHOST, domain, path)
        return flask.redirect(url)


class CreateAccount(MethodView):
    def get(self):
        return our_render('create-account.html')

    def post(self):
        email = flask.request.form.get('email')
        password = flask.request.form.get('password')
        success, msg = api.models.Account.register(email, password)
        if success:
            # TODO(Mek) Log user in here
            return flask.redirect('/create-page')
        else:
            return our_render('create-account.html', err_msg=msg)


class CreatePage(MethodView):
    def get(self):
        return our_render('create-page.html')


class Login(MethodView):
    def get(self):
        return our_render('login.html')


class Proxy(MethodView):

    def get(self, domain, path="index.html"):
        url = '{}/{}/{}'.format(HOST, domain, path)
        print(url)
        r = requests.get(url)
        print(r.content)
        return r.content
