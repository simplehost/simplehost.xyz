#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
:copyright: (c) 2018 by sabmek.
:license: see LICENSE for more details.
"""

from flask import Flask
from flask_routing import router
from flask_cors import CORS
from views import endpoints, api
import views
from configs import options, cors

urls = (
    '/p/<domain>/<path:path>', views.endpoints.Proxy,
    '/p/<domain>/', views.endpoints.Proxy,
    '/host', views.endpoints.Host,
    '/api/<cls>/<_id>', views.api.Router,
    '/api/<cls>', views.api.Router,
    '/api', views.api.Index,
    '/', views.Base,
    '/create-page', views.endpoints.CreatePage,
    '/create-account', views.endpoints.CreateAccount,
    '/login', views.endpoints.Login
)
app = router(Flask(__name__), urls)
cors = CORS(app) if cors else None

if __name__ == "__main__":
    app.run(**options)
