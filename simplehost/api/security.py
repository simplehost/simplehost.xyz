# -*- coding: utf-8 -*-

"""
    security
    ~~~~~~~~
    Generic User class for interfacing with use accounts +
    authentication.
"""

import hashlib
import random
from lepl.apps.rfc3696 import Email
from utils import Storage, ALPHANUMS


class Security(object):
    """The base Account class provides the basic functions for allowing
    user account creation and authentication, however it should be
    extended to allow user retrieval.
    """

    # not currently used
    RE_PASSWORD = r'([A-Za-z0-9%s]){%s,%s}$'

    @staticmethod
    def valid_email(email):
        return Email()(email)

    @staticmethod
    def sanitize_email(email):
        return email.lower().strip()

    @classmethod
    def authenticate(cls, email, passwd, salt, uhash):
        """Authenticates/validates a user's credentials by comparing
        their email and password versus their computed salted hash.
        A successful authentication results in True, False otherwise.
        """
        return cls._roast(email + salt + passwd) == uhash

    @classmethod
    def register(cls, email, passwd, passwd2=None, salt='', **kwargs):
        """Creates a complete user tuple according to seed
        credentials. The input undergoes a salting and roasting during
        a hashing process and all values are returned for further db
        handling (db insertion, etc). Note: For security reasons,
        plaintext passwords are never returned and should not be
        stored in a db unless there's a very specific reason to.

        XXX: Consider adding the following keyword arguments:
        password_validator - lambda for validating passwd (chars, len)

        :param salt: used for testing/verifying hash integrity in tests
        :param kwargs: any addt'l info which should be saved w/ the created user

        Usage:
            # typically, salt is not provided as an argument and is instead
            # generated as a side effect of Security.register("email@email.com", "password").
            # A salt has been included along with the following function call to
            # guarantee idempotence (i.e. a consistent/same uhash with each call)
            >>> Account.register("email@email.com", "password", salt="123456789")
            <Storage {'email': 'email@domain.com',
            'uhash': '021d98a32375ed850f459fe484c3ab2e352fc2801ef13eae274103befc9d0274',
            'salt': '123456789'}>

            # using additional kwargs, such as age (i.e. age=24) to add user attributes
            >>> Account.register("email@email.com", "password", salt="123456789", age=24)
            <Storage {'email': 'email@domain.com',
            'uhash': '021d98a32375ed850f459fe484c3ab2e352fc2801ef13eae274103befc9d0274',
            'salt': '123456789', 'age': 24}>
        """  # noqa
        email = cls.sanitize_email(email)

        if not cls.valid_email(email):
            raise ValueError("Email '%s' is malformed and does not "
                             "pass rfc3696." % email)

        if not passwd:
            raise ValueError('Password Required')

        # XXX Add password validation here.

        if passwd2 and not passwd == passwd2:
            raise ValueError('Passwords do not match')

        salt = salt or cls._salt()
        uhash = cls._roast(email + salt + passwd)
        fields = {
            'salt': salt,
            'uhash': uhash,
            'email': email
        }
        fields.update(kwargs)
        return Storage(fields)

    @classmethod
    def _salt(cls, length=12):
        """http://en.wikipedia.org/wiki/Salt_(cryptography)
        Salting results in the generation of random padding of a
        specified 'length' (12 default) which can be prepended or
        appended to a password prior to hashing to increase security
        and prevent against various brute force attacks, such as
        rainbow-table lookups."""
        return ''.join([random.choice(ALPHANUMS) for i in range(length)])

    @classmethod
    def _roast(cls, beans, chash=hashlib.sha256):
        """Computes a hash digest from email, salt, and
        password. Hot swappable algo in case there are code changes
        down the road."""
        return chash(beans.encode('utf-8')).hexdigest()
