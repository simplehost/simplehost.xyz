#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    api/models.py
    ~~~~~~~~~~~~~
    Simplehost API
    :copyright: (c) 2015 by mek.
    :license: see LICENSE for more details.
"""


from sqlalchemy.orm import relationship
from api import db, engine, core
from datetime import datetime
from sqlalchemy import Column, Unicode, BigInteger, Integer, \
        Boolean, DateTime, ForeignKey, Table, Index, exists, func
from sqlalchemy import MetaData
from api.security import Security


class Page(core.Base):
    __tablename__ = "pages"

    id = Column(BigInteger, primary_key=True)
    name = Column(Unicode, unique=True, nullable=False)
    page_id = Column(BigInteger, ForeignKey('pages.id'), nullable=False)
    account_id = Column(BigInteger, ForeignKey('accounts.id'), nullable=False)
    created = Column(DateTime(timezone=False), default=datetime.utcnow,
                     nullable=False)

class Resolver(core.Base):
    __tablename__ = "names"  # maps domain 2 page names

    id = Column(BigInteger, primary_key=True)
    domain = Column(Unicode, unique=True, nullable=False)  # must be lowercase/normalized
    page_id = Column(BigInteger, ForeignKey('pages.id'), nullable=False)
    activated = Column(Boolean, default=True)  # if they don't pay, deactivate
    page = relationship('Page', backref="domain")
    created = Column(DateTime(timezone=False), default=datetime.utcnow,
                     nullable=False)

class Account(core.Base):
    __tablename__ = "accounts"

    id = Column(BigInteger, primary_key=True)
    email = Column(Unicode, unique=True)
    salt = Column(Unicode)
    password_hash = Column(Unicode)
    enc_credit_card = Column(Unicode, unique=True, default=None)
    pages = relationship('Page', backref='account')
    created = Column(DateTime(timezone=False), default=datetime.utcnow,
                     nullable=False)

    def dict(self, verbose=False, minimal=False):
        u = super(Account, self).dict()
        del u['salt']
        del u['password_hash']
        return u

    @classmethod
    def register(cls, email, password):
        """
        (email, password) => (Boolean, msg)
        """
        try:
            r = Security.register(email, password)
            act = cls(email=r.email,
                      salt=r.salt,
                      password_hash=r.uhash)
            act.create()
            return (True, "")
        except ValueError as e:
            return (False, str(e))

# This builds a dictionary of all of skillsera types
# in core.modes (which is used in views)
for model in core.Base._decl_class_registry:
    m = core.Base._decl_class_registry.get(model)
    try:
        core.models[m.__tablename__] = m
    except:
        pass
