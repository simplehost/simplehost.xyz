#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    __init__.py
    ~~~~~~~~~~~

    :copyright: (c) 2018 by sabmek.
    :license: see LICENSE for more details.
"""

__title__ = 'simplehoster'
__version__ = '0.0.1'
__author__ = 'sabmek'


from sqlalchemy import create_engine
from sqlalchemy.orm import scoped_session, sessionmaker
from configs import DB_URI, DEBUG

engine = create_engine(DB_URI, echo=DEBUG, client_encoding='utf8')
db = scoped_session(sessionmaker(bind=engine))

from . import models
