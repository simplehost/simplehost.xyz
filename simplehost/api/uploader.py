#!/usr/bin/env python
# -*- coding: utf-8 -*-

import tempfile
import sys
import urllib
try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO
import internetarchive as ia
from configs import s3_access, s3_secret
import flatdb

DATABASE = 'database.tsv'


def assert_ownership(fqdn, passcode):
    db = flatdb.FlatDatabase(DATABASE)
    pred = db.get(fqdn) is None or db.keyeq(fqdn, passcode)
    assert pred, "Incorrect passcode for fqdn {}".format(fqdn)


def set_ownership(fqdn, passcode):
    return flatdb.FlatDatabase(DATABASE).set(fqdn, passcode)


def upload(domain, content, filepath='index.html', passcode=None):
    """Uploads content to archive.org within the item simplehost
    within the directory "domain"

    Usage:
        >>> upload("google.com", "<html><body><h1>Hello World</h1></body></html>", filepath="index.html")

    https://archive.org/download/simplehost.xyz/google.com/index.html

    """  # noqa
    fqdn = canonicalize(domain)
    assert_ownership(fqdn, passcode)

    with tempfile.NamedTemporaryFile(mode='w+', suffix='.html', delete=True) as f:
        print(content)
        f.write(content)
        f.seek(0)
        item = ia.upload('simplehost.xyz',
                         {'%s/%s' % (domain, filepath): f.name},
                         access_key=s3_access, secret_key=s3_secret)
    set_ownership(fqdn, passcode)
    return item


def canonicalize(domain):
    if '://' in domain:
        return urllib.parse.urlparse(domain).netloc
    else:
        return domain.split('/')[0]


if __name__ == "__main__":
    domain = sys.argv[1]
    html = sys.argv[2]
    upload(domain, html)
