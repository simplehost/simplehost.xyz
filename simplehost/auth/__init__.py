#!/usr/bin/env python
# -*- coding: utf-8 -*-

"""
    __init__.py
    ~~~~~~~~~~~

    :copyright: (c) 2018 by sabmek.
    :license: see LICENSE for more details.
"""

__title__ = 'bonsauth'
__version__ = '0.0.1'
__author__ = 'sabmek'
