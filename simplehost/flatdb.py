"""
flatdb.py
~~~~~~~~~

Stores data in a tsv.

:copyright: (c) 2018 by sabmek
:license: see LICENSE for more details.
"""

try:
    from StringIO import StringIO
except ImportError:
    from io import StringIO

import csv
import json


FMT_TSV = '.tsv'
FMT_JSON = '.json'


def tsv_loads(content):
    rd = csv.reader(StringIO(content), delimiter="\t", quotechar='"')
    d = {}
    for line in rd:
        k, v = line
        d[k] = v
    return d


def tsv_dumps(dictionary):
    st = StringIO()
    writer = csv.writer(st, delimiter='\t', lineterminator='\n')
    for (k, v) in dictionary.items():
        writer.writerow([k, v])
    return st.getvalue()


class FlatDatabase(object):
    def __init__(self, path, fmt=FMT_TSV):
        self.path = path
        self.fmt = fmt
        self._dict = {}

    def _parse(self, content):
        if self.fmt == FMT_TSV:
            return tsv_loads(content)
        elif self.fmt == FMT_JSON:
            return json.loads(content)

    def _dump(self, fmt=None):
        fmt = fmt if fmt else self.fmt
        if fmt == FMT_TSV:
            return tsv_dumps(self._dict)
        elif fmt == FMT_JSON:
            return json.dumps(self._dict)

    def _save(self):
        with open(self.path, 'w+') as f:
            f.write(self._dump(fmt=self.fmt))

    def _load(self):
        try:
            with open(self.path) as f:
                self._dict = self._parse(f.read())
        except IOError:
            self._dict = {}

    def set(self, key, val):
        self._dict[key] = val
        self._save()

    def get(self, key):
        self._load()
        return self._dict.get(key)

    def keyeq(self, key, val):
        return self.get(key) == val
