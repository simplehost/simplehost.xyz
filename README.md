# Bonsai

https://docs.google.com/document/d/1nHYnub7skBGX3q4Tp-aQz66D-kpgilDSxnnspEz5XiQ/edit

# DevOps

## Deploy Service

## Start/Stop/Restart Service

See doc for password.

```
ssh simplehost@server.openjournal.foundation
tmux
cd /var/www/simplehost.xyz/simplehost
python app.py
tmux -d
```

## Example Usage

$ python simplehoster/uploader.py google.com "<html><body><h1>Hello World</h1></body></html>"

## Update our Index

ia configure
email: simplehost.xyz@gmail.com
password: ****
ia upload simplehost.xyz index.html
